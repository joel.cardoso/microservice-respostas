package com.curso.microservices.app.respostas.service;

import com.curso.microservices.app.respostas.model.entity.Resposta;

public interface RespostaService {
	
	public Iterable<Resposta> saveAll(Iterable<Resposta> respostas);
	
	public Iterable<Resposta> findRespostaByAlunoByExame(Long alunoId, Long exameId);
	
	public Iterable<Long> findExamesIdsRespondidosByAluno(Long alunoId);
}
