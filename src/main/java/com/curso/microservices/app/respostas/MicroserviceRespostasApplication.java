package com.curso.microservices.app.respostas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
@EntityScan({"com.curso.microservices.app.respostas.model.entity", "com.curso.microservices.commons.alunos.models.entity", "com.curso.microservices.commons.exames.models.entity"})
public class MicroserviceRespostasApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroserviceRespostasApplication.class, args);
	}

}
