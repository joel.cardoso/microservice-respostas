package com.curso.microservices.app.respostas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.curso.microservices.app.respostas.model.entity.Resposta;
import com.curso.microservices.app.respostas.model.repository.RespostaRepository;

@Service
public class RespostaServiceImpl implements RespostaService{
	
	@Autowired
	private RespostaRepository repository;
	
	@Override
	@Transactional
	public Iterable<Resposta> saveAll(Iterable<Resposta> respostas) {
		return repository.saveAll(respostas);
	}
	
	@Transactional(readOnly = true)
	@Override
	public Iterable<Resposta> findRespostaByAlunoByExame(Long alunoId, Long exameId) {
		return repository.findRespostaByAlunoByExame(alunoId, exameId);
	}
	
	@Transactional(readOnly = true)
	public Iterable<Long> findExamesIdsRespondidosByAluno(Long alunoId){
		return repository.findExamesIdsRespondidosByAluno(alunoId);
	}
}
