package com.curso.microservices.app.respostas.model.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.curso.microservices.app.respostas.model.entity.Resposta;

@Repository
public interface RespostaRepository extends CrudRepository<Resposta, Long>{
	
	@Query("select r from Resposta r join fetch r.aluno a join fetch r.pergunta p join fetch p.exame e where a.id=?1 and e.id=?2")
	public Iterable<Resposta> findRespostaByAlunoByExame(Long alunoId, Long exameId);
	
	@Query("select e.id from Resposta r join r.aluno a join r.pergunta p join p.exame e where a.id=?1 group by e.id")
	public Iterable<Long> findExamesIdsRespondidosByAluno(Long alunoId);
}
