package com.curso.microservices.app.respostas.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.curso.microservices.app.respostas.model.entity.Resposta;
import com.curso.microservices.app.respostas.service.RespostaService;

@RestController
public class RespostaController {
	
	@Autowired
	private RespostaService service;
	
	@PostMapping
	public ResponseEntity<?> save(@RequestBody Iterable<Resposta> respostas) {
		Iterable<Resposta> respostaDB = service.saveAll(respostas);
		return ResponseEntity.status(HttpStatus.CREATED).body(respostaDB);
	}
	
	@GetMapping("/aluno/{alunoId}/exame/{exameId}")
	public ResponseEntity<?> obterRespostasPorAlunoPorExame(@PathVariable Long alunoId, @PathVariable Long exameId){
		Iterable<Resposta> respostas = service.findRespostaByAlunoByExame(alunoId, exameId);
		return ResponseEntity.ok(respostas);
	}
	
	@GetMapping("/aluno/{alunoId}/exames-respondidos")
	public ResponseEntity<?> obterExamesIdComRespostasAluno(@PathVariable Long alunoId) {
		Iterable<Long> examesIds = service.findExamesIdsRespondidosByAluno(alunoId);
		return ResponseEntity.ok(examesIds);
	}
}
